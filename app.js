var express = require('express');  // módulo express
var app = express();		   // objeto express
var server = require('http').Server(app);
var socketio = require('socket.io')(server);
//var functions = require('public/js/functions').functions; //Não consegui referenciar
var bodyParser = require('body-parser');  // processa corpo de requests
var cookieParser = require('cookie-parser');  // processa cookies
var irc = require('irc');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded( { extended: true } ));
app.use(cookieParser());
app.use(express.static('public'));

var path = require('path');	// módulo usado para lidar com caminhos de arquivos

var proxies = {}; // mapa de proxys
var proxy_id = 0;
var millisecondsToWait = 1000;
	setTimeout(function() {
		timestamp = Date.now();	
		console.log(timestamp_to_date(timestamp));
		//socket.emit("Estou Enviando" , {"timestamp":Date.now()});
	}, millisecondsToWait);


function proxy(id, servidor, nick, canal, socket) {
// 	var cache = []; // cache de mensagens
	var ws = socket;
	var irc_client = new irc.Client(
			servidor, 
			nick,
			{channels: [canal],});						

	irc_client.addListener('message'+canal, function (from, message) {
	    console.log(from + ' => '+ canal +': ' + message);
	    /* cache.push(	{"timestamp":Date.now(), 
			"nick":from,
			"msg":message} );*/
	    ws.emit(	"message", {"timestamp":Date.now(), 
			"nick":from,
			"msg":message} );
	});
	irc_client.addListener('error', function(message) {
	    console.log('error: ', message);
	});
	irc_client.addListener('mode', function(message) {
	    console.log('mode: ', message);
	});
	proxies[id] = { "ws":socket, "irc_client":irc_client  };

	return proxies[id];
}

app.get('/', function (req, res) {
  if ( req.cookies.servidor && req.cookies.nick  && req.cookies.canal ) {
	proxy_id++;
	socketio.on("connection", function (socket) {
		var p =	proxy(	proxy_id,
				req.cookies.servidor,
				req.cookies.nick, 
				req.cookies.canal,
				socket);
		socket.on("message", function (message) {
			console.log("mensagem:"+message);
			proxies[proxy_id].irc_client.say(req.cookies.canal, message);
		})
		socket.on('chat', function () {
			setTimeout(function() {
				timestamp = Date.now();	
				console.log(timestamp_to_date(timestamp));
				socket.emit('result' , timestamp_to_date(timestamp))
			})
			})	
	});
	res.cookie('id', proxy_id);
  	res.sendFile(path.join(__dirname, '/index.html'));
  }
  else {
        res.sendFile(path.join(__dirname, '/login.html'));
  }
});

/*
app.get('/obter_mensagem/:timestamp', function (req, res) {
  var id = req.cookies.id;
  res.append('Content-type', 'application/json');
  res.send(proxies[id].cache);
});
*/

app.post('/gravar_mensagem', function (req, res) {
  // proxies[req.cookies.id].cache.push(req.body);
  var irc_client = proxies[req.cookies.id].irc_client;
  irc_client.say(req.cookies.canal, irc_client.opt.channels[0], req.body.msg );
  res.end();
});

app.get('/mode/:usuario/:args', function (req, res){
  var usuario = req.params.usuario;
  var args = req.params.args;
  //var retorno = '{"usuario":'+usuario+','+
  //		  '"args":"'+args+'}';
  
  var irc_client = proxies[req.cookies.id].irc_client;
  var retorno = irc_client.send("mode", usuario, args);
  res.send(retorno);
});
app.get('/mode/', function (req, res){
  
  var irc_client = proxies[req.cookies.id].irc_client;
  var retorno = irc_client.send("mode", req.cookies.nick);
  res.send(retorno);
});


app.post('/login', function (req, res) { 
   res.cookie('nick', req.body.nome);
   res.cookie('canal', req.body.canal);
   res.cookie('servidor', req.body.servidor);
   res.redirect('/');
});

function timestamp_to_date( timestamp ) {
	var date = new Date( timestamp );
	var hours = date.getHours();
	var s_hours = hours < 10 ? "0"+hours : ""+hours;
	var minutes = date.getMinutes();
	var s_minutes = minutes < 10 ? "0"+minutes : ""+minutes;
	var seconds = date.getSeconds();
	var s_seconds = seconds < 10 ? "0"+seconds : ""+seconds;
	return s_hours + ":" + s_minutes + ":" + s_seconds;
}

server.listen(3000, function () {				
  console.log('Example app listening on port 3000!');	
});
